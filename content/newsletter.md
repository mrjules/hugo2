---
title: "newsletter"
date: 2018-02-13T13:42:49-05:00
---

Ut mollis vitae justo id ornare. Etiam interdum, lorem non cursus porttitor.

<div id="newsletter">
	<form action="https://tinyletter.com/sample" method="post"
		target="popupwindow" onsubmit="window.open('https://tinyletter.com/example', 'popupwindow', 'scrollbars=yes,width=800,height=800');return true">
		<p>
			<input type="text" style="width:60%" name="email" id="tlemail" placeholder="Your email address" id="newsletter-submit" />
		</p>
		<input type="hidden" value="1" name="embed" />
		<input type="submit" value="Subscribe" />
	</form>
</div>